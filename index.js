
// Get Dependencies/Modules
const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require('./routes/userRoutes');

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Connect to MongoDB
mongoose.connect('mongodb+srv://admin:admin1234@zuitt-bootcamp.qrju6.mongodb.net/s31-36?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true
});


app.use('/api/users',userRoutes); 



// Server listening
app.listen(port,()=>console.log(`Listening to port ${port}`));